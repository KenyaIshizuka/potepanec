require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy_id: 1, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path taxon.id
  end

  scenario "display category page" do
    within("h2.taxon_name") do
      expect(page).to have_content taxon.name
    end

    within("li.taxon_name") do
      expect(page).to have_content taxon.name
    end
  end

  scenario "display taxonomy name in sidebar" do
    expect(page).to have_content taxonomy.name
  end

  scenario "display taxon name in sidebar and link to taxon page" do
    within(".taxonomy-name") do
      expect(page).to have_content taxon.name
    end

    within(".taxonomy-link") do
      click_link taxon.name
    end

    expect(current_path).to eq potepan_category_path taxon.id
  end

  scenario "display product and link to product's page" do
    within(".product-name-link h5.product-name") do
      expect(page).to have_content product.name
    end

    find(".product-link-wrapper").click

    expect(current_path).to eq potepan_product_path product.id
  end
end
