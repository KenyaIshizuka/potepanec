require 'rails_helper'

RSpec.feature "Projects", type: :feature do
  let(:product) { create(:product) }

  before do
    visit potepan_product_path product.id
  end

  scenario "visit from product page to top page" do
    click_link "HOME", match: :first
    expect(current_path).to eq potepan_root_path
  end

  scenario "has proper page title" do
    expect(page).to have_title "#{product.name} | BIGBAG"
  end

  scenario "has product's data" do
    within(".page-title h2") do
      expect(page).to have_content product.name
    end

    within(".breadcrumb li.active") do
      expect(page).to have_content product.name
    end

    within(".media-body") do
      within("h2") do
        expect(page).to have_content product.name
      end

      within("p") do
        expect(page).to have_content product.description
      end

      within("h3") do
        expect(page).to have_content product.display_price
      end
    end
  end
end
