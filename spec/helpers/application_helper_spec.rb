require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe 'full_title(page_title)' do
    it "page_titleがない場合" do
      expect(full_title("")).to eq ApplicationHelper::BASE_TITLE
    end

    it "page_titleがnilの場合" do
      expect(full_title(nil)).to eq ApplicationHelper::BASE_TITLE
    end

    it "page_titleが存在する場合" do
      expect(
        full_title("RUBY ON RAILS TOTE")
      ).to eq "RUBY ON RAILS TOTE | #{ApplicationHelper::BASE_TITLE}"
    end
  end
end
