require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "potepan/products" do
    let(:product) { create(:product) }

    before do
      get potepan_product_path(product.id)
    end

    it "includes product_name" do
      expect(response.body).to include(product.name)
    end

    it "includes product_description" do
      expect(response.body).to include(product.description)
    end

    it "includes product_price" do
      expect(response.body).to include(product.display_price.to_s)
    end
  end
end
