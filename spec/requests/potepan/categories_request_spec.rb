require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "show template" do
    let(:taxon) { create(:taxon) }
    let(:taxonomy) { create(:taxonomy) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "returns a 200 response" do
      expect(response).to have_http_status 200
    end

    it "returns show template" do
      expect(response).to render_template :show
    end

    it "includes taxon name" do
      expect(response.body).to include(taxon.name)
    end

    it "includes taxonomies name" do
      expect(response.body).to include(taxonomy.name)
    end
  end
end
